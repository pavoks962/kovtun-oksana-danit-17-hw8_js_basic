"use strics"
 

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const pList = document.getElementsByTagName('p');   
for (let node of pList) {
node.classList.add('color-replace') 
}

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const idOptionList = document.getElementById('optionsList');  

console.log(idOptionList);
console.log(idOptionList.parentNode);
for(let node of idOptionList.childNodes) {
    console.log(node.nodeName);
    console.log(node.nodeType)
};

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph

document.getElementById('testParagraph').innerHTML='This is a paragraph';   



// 4. Отримати елементи, вкладені <li> в елемент із класом main-header і вивести їх у консоль. 
// Кожному з елементів присвоїти новий клас nav-item.

const elementMainHeader = document.querySelectorAll('.main-header li');

elementMainHeader.forEach((element) => {
    console.log(element);
    element.classList.add('nav-item');
})

console.log(elementMainHeader)

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const elementSectionTitle = document.querySelectorAll('.section-title')      
elementSectionTitle.forEach((elem) => {
    
    elem.classList.remove('section-title');
    console.log(elem)
})

